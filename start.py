
import folium
import psycopg2

connection = psycopg2.connect(user=' ',
                              # пароль, который указали при установке PostgreSQL
                              password=' ',
                              host=' ',
                              port=' ',
                              database='grafana_t')
cursor = connection.cursor()
cursor.execute("""select distinct * from earth e 
                    left join weat_ear w
                    on ROUND (e.latitude,4)=ROUND (w.lat,4) and ROUND (e.longitude,4)=ROUND (w.lon,4)
                    where date_trunc('day',datar)=current_date""")


# color earthquake magnitude point
def color_change(mag):
    if mag < 3:
        return ('green')
    elif (3 <= mag < 5):
        return ('yellow')
    elif (5 <= mag < 7):
        return ('orange')
    else:
        return ('red')


# shake magnitude poin
def shake_poin():
    for i in cursor.fetchall():
        folium.CircleMarker(location=[i[0], i[1]], radius=4,
                            popup=(i[2], 'mag=' + str(i[3]), + 10 * '-', 'Weather',
                            str(i[7]) + 'ºC', 10 * '-', 'Near loc ' + str(i[9])),
                            fill_color=color_change(float(i[3])), color="gray", fill_opacity=10).add_to(map)


url = "https://earthquake.usgs.gov/fdsnws/event/1/query?"
url2 = 'https://raw.githubusercontent.com/fraxen/tectonicplates/master/GeoJSON/PB2002_boundaries.json'

# TileLayer
map = folium.Map(location=[0, 0], zoom_start=2.6, tiles="OpenStreetMap")

# Tictonic plate
folium.GeoJson(url2, name='geojson').add_to(map)
folium.TileLayer('cartodbdark_matter').add_to(map)
folium.LayerControl().add_to(map)

shake_poin()

map.save("map2.html")
