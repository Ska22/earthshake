from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.postgres_operator import PostgresOperator
import requests

args = {
    'owner': 'Lost',
    'start_date': datetime(2023, 4, 5),
    'provide_context': True
}


API_KEY = ''
BASE_URL = 'https://api.openweathermap.org/data/2.5/weather?'


def extract_weather():
    query = """select * from earth
    where date_trunc('day',datar)=current_date"""
    hook = PostgresHook(postgres_conn_id='grafana_t')
    conn = hook.get_conn()
    cur = conn.cursor()
    cur.execute(query)
    for i in cur.fetchall():
        a = requests.get(BASE_URL,
                         params={'lat': float(i[0]), 'lon': float(i[1]), 'units': 'metric', 'lang': 'ru',
                                 'APPID': API_KEY})
        b = a.json()
        query1 = f"""insert into weat_ear_stg (lat,lon,temp,date,name)
                          values (
                                '{b['coord']['lat']}',
                                '{b['coord']['lon']}',
                                '{b['main']['temp']}',
                                '{str(datetime.fromtimestamp(b['dt']))}',
                                '{b['name']}')
                             """

        cur.execute(query1)
    conn.commit()


with DAG('Am12324532523521', description='gfasgfdsfgdfgdf', schedule_interval='1 * * * *', catchup=False,
         default_args=args) as dag:
    create_weather = PostgresOperator(
        task_id='create_weather',
        postgres_conn_id='grafana_t',
        sql="""CREATE TABLE if not exists weat_ear(
                            lat numeric not null,
                            lon numeric not null,
                            temp numeric not null,
                            date timestamp not null,
                            name varchar(100) not null);

                            CREATE TABLE if not exists weat_ear_stg(
                            lat numeric not null,
                            lon numeric not null,
                            temp numeric not null,
                            date timestamp not null,
                            name varchar(100) not null);
                            
                           /* Clean support table  */
                            DELETE FROM weat_ear_stg;
                    """)
    extract_weather = PythonOperator(task_id='extract_weather', python_callable=extract_weather)

    load_weat = PostgresOperator(
        task_id='load_weat',
        postgres_conn_id='grafana_t',
        sql="""MERGE INTO weat_ear TGT
    USING (
    SELECT
     s.lat,s.lon,s.temp,s.date,s.name,
     t.lat as tla,t.lon as tlo,t.temp as tt,t.date as tp,t.name as tm
    FROM weat_ear_stg S
    LEFT JOIN weat_ear T
    ON s.lat = t.lat and s.lon=t.lon
    where
        t.lat is null or
        (t.lat is not null and ( 1=0
            or t.lon <> s.lon or ( s.lon is null and t.lon is not null ) or ( s.lon is not null and t.lon is null )
            or t.temp <> s.temp or ( s.temp is null and t.temp is not null ) or ( s.temp is not null and t.temp is null )
            --or t.date <> s.date or ( s.date is null and t.date is not null ) or ( s.date is not null and t.date is null )
            or t.name <> s.name or ( s.name is null and t.name is not null ) or ( s.name is not null and t.name is null )
            )
        )
    ) STG
    ON (tgt.lat = stg.lat and tgt.lon=stg.lon)
    WHEN NOT MATCHED THEN INSERT (lat,lon,temp,date,name)
							values (stg.lat,stg.lon,stg.temp,stg.date,stg.name)

        """)

    create_weather >> extract_weather>>load_weat
