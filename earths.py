import requests
import json
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.models import Variable

args = {
    'owner': 'Lost',
    'start_date': datetime(2023, 4, 5),
    'provide_context': True
}

url = 'https://earthquake.usgs.gov/fdsnws/event/1/query?'



def extract(**kwargs):
    ti = kwargs['ti']
    params = {'format': 'geojson',
              'starttime': datetime.today() - timedelta(days=1),
              'endtime': datetime.today()}
    m1 = requests.get(url, params=params)
    m = json.loads(m1.text)
    ti.xcom_push(key="earthshake", value=m)


def transform(**kwargs):
    ti = kwargs['ti']
    m = ti.xcom_pull(key='earthshake', task_ids=['extract'])
    current_value = []
    for i in m[0]['features']:
        current_value.append(
            [i['geometry']['coordinates'][1],
             i['geometry']['coordinates'][0],
             i['properties']['place'],
             i['properties']['mag'],
             str(datetime.fromtimestamp(int(str(i['properties']['time'])[:-3])))])
    current_value.append(m[0]['metadata']['count'])

    ti.xcom_push(key='earthshake', value=current_value)
    xcom_var = Variable.set("xcom_var", ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][-1])

with DAG('Am2221', description='earthshake', schedule_interval='1 * * * *', catchup=False, default_args=args) as dag:
    extract = PythonOperator(task_id='extract', python_callable=extract)
    transform = PythonOperator(task_id='transform', python_callable=transform)
    create = PostgresOperator(
        task_id='create',
        postgres_conn_id="grafana_t",
        sql="""     /*Сreate main table*/
                        Create table if not exists earth(
                        latitude numeric not null,
                        longitude numeric not null,
                        place varchar(200) not null,
                        mag varchar(30) not null,
                        Datar timestamp not null);
                
                        Create table if not exists earth_stg(
                        latitude numeric not null,
                        longitude numeric not null,
                        place varchar(200) not null,
                        mag varchar(30) not null,
                        Datar timestamp not null);
                         
                    /* Clean support table  */
                        DELETE FROM earth_stg;
                         """)

    load = PostgresOperator(
        task_id='load',
        postgres_conn_id='grafana_t',
        sql=[f"""insert into earth_stg (latitude,longitude,place,mag,Datar)
                            values
                            ({{{{ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][{i}][0]}}}},
                            {{{{ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][{i}][1]}}}},
                            '{{{{ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][{i}][2]}}}}',
                            {{{{ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][{i}][3]}}}},
                            to_timestamp('{{{{ti.xcom_pull(key='earthshake', task_ids=['transform'])[0][{i}][4]}}}}','YYYY-MM-DD HH24:MI:SS')                          
                            );
                            """ for i in range(int(Variable.get('xcom_var')))])

    load_main = PostgresOperator(
        task_id='load_main',
        postgres_conn_id='grafana_t',
        sql=["""
        MERGE INTO earth TGT
    USING (
    SELECT
     s.latitude,s.longitude,s.place,s.mag,s.Datar,
     t.latitude as tla,t.longitude as tlo,t.place as tp,t.mag as tm,t.Datar as tdt
    FROM earth_stg S
    LEFT JOIN earth T
    ON s.latitude = t.longitude and s.datar=t.datar
    where
        t.latitude is null or
        (t.latitude is not null and ( 1=0
            or t.longitude <> s.longitude or ( s.longitude is null and t.longitude is not null ) or ( s.longitude is not null and t.longitude is null )
            or t.place <> s.place or ( s.place is null and t.place is not null ) or ( s.place is not null and t.place is null )
            or t.mag <> s.mag or ( s.mag is null and t.mag is not null ) or ( s.mag is not null and t.mag is null )
            or t.DATAr <> s.DATAr or ( s.DATAr is null and t.DATAr is not null ) or ( s.DATAr is not null and t.DATAr is null )
            )
        )
    ) STG
    ON (tgt.latitude = stg.latitude and tgt.mag=stg.mag)
    WHEN NOT MATCHED THEN INSERT (latitude,longitude,place,mag,Datar)
							values (stg.latitude,stg.longitude,stg.place,stg.mag,stg.Datar)
        
        """]
    )

    extract >> transform >> create >> load >> load_main

